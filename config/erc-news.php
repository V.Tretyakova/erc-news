<?php

return [
    'route' => '/erc-news',
    'middleware' => ['auth'],
    'html_login_hint' => '<b> Первая строка уведомления администратора <br/> Вторая строка уведомления администратора </br>',
    'html_login_info' => '<li>Первая строка уведомления администратора</li><li>Вторая строка уведомления администратора</li>',
    'password_expiry_field' => 'expiry_time',
    'password_expiry_notice_message' => 'До истечения работы вашего пароля осталось :?. Вы можете изменить его сейчас ',
    'password_expiry_notice_period' => 14,
    'password_change_url' => '/change_password'
];
