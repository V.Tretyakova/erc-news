<?php
namespace ERCDEV\ErcNews;

use Illuminate\Support\ServiceProvider;

class ErcNewsServiceProvider extends ServiceProvider
{
    protected $defer = false;

    public function boot()
    {
        if (! $this->app->routesAreCached()) {
            require __DIR__ . '/Http/routes.php';
        }

        $this->publishes([__DIR__ . '/../config/erc-news.php' => config_path('erc-news.php')], 'erc-news');

        $this->loadViewsFrom(__DIR__.'/../resources/views', 'erc-news');

        $this->publishes([
            __DIR__.'/../resources/views' => base_path('resources/views/vendor/erc-news'),
        ]);
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/erc-news.php', 'erc-news');
    }

    /**
     * @return array
     */
    public function provides()
    {
        return ['erc-news'];
    }
}
