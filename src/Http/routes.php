<?php

$middleware = config('erc-news.middleware');
$route = config('erc-news.route');

Route::get($route, ['middleware' => $middleware, 'as' => 'erc.news', 'uses' => \ERCDEV\ErcNews\Http\Controllers\ErcNewsController::class . "@index"]);
