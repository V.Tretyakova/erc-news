<?php
namespace ERCDEV\ErcNews\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Routing\Controller;

class ErcNewsController extends Controller
{
    function str_replace_once($search, $replace, $text)
    {
        $pos = strpos($text, $search);
        return $pos !== false ? substr_replace($text, $replace, $pos, strlen($search)) : $text;
    }

    function index()
    {
        $notice_period = config('erc-news.password_expiry_notice_message');
        $string = config('erc-news.password_expiry_notice_period');
        if(auth()->user()->config('erc-news.password_expiry_field')){
            $expiry_time = Carbon::createFromFormat('Y-m-d H:i:s', auth()->user()->config('erc-news.password_expiry_field'));
            $diff = Carbon::now()->diff($expiry_time);
            if($diff->days > $notice_period){
                $diff = false;
            } else{
                if ($diff->days === 0){
                    $diff = $this->str_replace_once(':?', $diff->h . ' ч', $string);
                } else{
                    $diff = $this->str_replace_once(':?', $diff->days . ' д', $string);
                }
            }
        } else{
            $diff = false;
        }
        return view('erc-news', ['diff' => $diff]);
    }
}
