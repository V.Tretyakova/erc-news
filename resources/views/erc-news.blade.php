<div class="container">
    <div class="row">
        <div class="col-md-12">
            <style type="text/css">
                .news_list li, .warning_list {
                    list-style: none;
                    padding: 20px;
                    width: 100%;
                    box-shadow: 0px 0px 10px #0000003b;
                    text-align: left;
                    margin-bottom: 15px;
                }

                .news_list li:hover {
                    box-shadow: 0px 0px 10px rgba(127, 139, 144, 0.47);
                    background: #d9edf7;
                    cursor: pointer;
                }

                .warning_list {
                    background: #ffd777;
                    text-align: center;
                }

                .news_list {
                    padding: 0 25px;
                }
            </style>
            <div class="panel panel-info">
                <div class="panel-heading">Новости</div>
                <div class="panel-body">
                    @if($diff)
                        <div class="warning_list">
                            {{$diff}}<a type="button" class="btn btn-warning" href="{{config('erc-news.password_change_url')}}"
                                        style="cursor: pointer;">Изменить</a>
                        </div>
                    @endif
                    <ul class="news_list">
                        <?php echo $html_login_info; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>