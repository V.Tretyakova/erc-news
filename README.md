## Goal
Display information to the user about the expiration of the login password and critical news of the system.

## Considerations
### Routes
This package adds the routes under `ercdev/erc-news`. Please verify that these don't collide with your
existing routes.

### Dependencies
- Your project should be running Laravel 5.1 or higher.

## Installation
1. Install via composer:

   ```sh
       composer require ercdev/erc-news:~1.0.0
   ```
 
2. Publish configurations and resources:
   ```sh
       php artisan vendor:publish
   ```
   
3. Add the service provider entry in `config\app.php`:
   ```php
       ERCDEV\ERCNews\ERCNewsServiceProvider::class,
   ```

4. Include `erc-news.blade.php` on your custom view:
   ```php
       @include('vendor.erc-news.erc-news')
   ```

## Configuration
The following elements are configurable:
- **route:** (default: `erc-news`) Change to customize in the browser. This is just cosmetic.
- **middleware:** (default: `['auth']`) Change to `[]` if you doesn`t check user access.
- **html_login_hint:** (default: `<b> First <br/> Second </br>`) Change to display news in paragraph formatting on top of block.
- **html_login_info:** (default: `<li>First</li><li>Second</li>`) Change to display news in lists formatting on bottom of block.
- **password_expiry_field:** (default: `expiry_time`) Change this to column name in your db of users table.
- **password_expiry_notice_message:** (default: `Your password expired on :?. You can change it now `) Change to display notification of expiring password.
- **password_expiry_notice_period:** (default: `14`) Change to display notification notice period.
- **password_change_url:** (default: `/change_password`) Change to display url for changing password.

You can now change the default value in `/app/config/erc-news.php` as desired. Deleting the
`/app/config/erc-news.php` file will revert back to the default settings.

## Usage
That was it!
